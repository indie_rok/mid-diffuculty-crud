// JavaScript Document

$("#dropdownEstados").change(function(){
	$("#dropdownLocalidad").empty();
	$("#dropdownMunicipios").empty();
	var estado = $( "#dropdownEstados" ).find(":selected").val();
	$.ajax({
         type: "POST",
         url: 'ajaxLocalidad', 
         data: {estado:estado},
         dataType: "text",  
         cache:false,
         success: 
              function(data){
				
				var optionValues = $.parseJSON(data);
				
				var opts;
				
				for (var i = 0; i < optionValues.length; i++) {
					opts += "<option value='" + optionValues[i].cve_mun + "'>" + optionValues[i].nom_mun + "</option>";
				}
				$("#dropdownLocalidad").append(opts);	
				
              }
          });
     return false;
}); 

$("#dropdownLocalidad").change(function(){
	$("#dropdownMunicipios").empty();
	var estado = $( "#dropdownEstados" ).find(":selected").val();
	var localidad = $("#dropdownLocalidad").find(":selected").val();
		
		$.ajax({
			 type: "POST",
			 url: 'ajaxMunicipio', 
			 data: {estado: estado,localidad:localidad},
			 dataType: "text",
			 cache:false,
			 success: 
				  function(data){
					
					var optionValues = $.parseJSON(data);
					var opts;
					
					for (var i = 0; i < optionValues.length; i++) {
						opts += "<option value='" + optionValues[i].localidad_cve + "'>" + optionValues[i].localidad_nombre + "</option>";
					}
					$("#dropdownMunicipios").append(opts);	
					
				  }
			  });
		 return false;
}); 


$("#dropdownDepartamento").change(function(){
	$("#dropdownArea").empty();
	var departamento = $( "#dropdownDepartamento" ).find(":selected").val();
	$.ajax({
         type: "POST",
         url: 'usuario/ajaxArea', 
         data: {departamento:departamento},
         dataType: "text",  
         cache:false,
         success: 
              function(data){
				
				var optionValues = $.parseJSON(data);
				
				var opts;
				
				for (var i = 0; i < optionValues.length; i++) {
					opts += "<option value='" + optionValues[i].id_a + "'>" + optionValues[i].nombre_a + "</option>";
				}
				$("#dropdownArea").append(opts);	
				
              }
          });
     return false;
}); 


