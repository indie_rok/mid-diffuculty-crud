<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('dropdownDinamico'))
{
    function dropdownDinamico($options,$defaultOption = true)
    {
		$noAssociativeArray = array("");
		foreach ($options as $item) {
			$item = array_values($item);
			array_push($noAssociativeArray,$item);
		}
		
		if($defaultOption == true){
			echo "<option>Seleccione</option>";
		}
		
		for($i=0;$i<count($noAssociativeArray);$i++){
			for($k=0;$k<(count($noAssociativeArray[$i]))-1;$k++){
				echo "<option value=".$noAssociativeArray[$i][$k].">".$noAssociativeArray[$i][$k+1]."</option>";
			}
		}
    }   
}