<?php 

class consultasmodel extends CI_Model{
	
	public function __construct(){
		parent:: __construct();		
	}
	
	public function getConsultasPendientes(){
		$this->db->where("estatus_con","pendiente");
		$consultas = $this->db->get("consultas",20,10);
		$arrayPacientes = $consultas->result_array();
		
		
		for($i=0;$i<count($arrayPacientes);$i++){
			$pacienteName = $this->getPacienteName($arrayPacientes[$i]["id_paciente_con"]);
			$arrayPacientes[$i]["nombreCompleto"] = $pacienteName;
		}	
		
		return ($arrayPacientes);
	}
	
	public function getPacienteId($consultaID){
		$this->db->select("id_paciente_con");
		$this->db->where("id_con",$consultaID);
		$idUsuario = $this->db->get("consultas");
		$array = $idUsuario->row_array();	
		return $array["id_paciente_con"];
	}
	
	public function getPacienteName($id){
		$this->db->select("nombre_paciente,apellido_paterno_paciente,apellido_materno_paciente");
		$this->db->where("id_p",$id);
		$paciente  = $this->db->get("pacientes");
		$nombreArray = $paciente->row_array();
		return @$nombreArray["apellido_paterno_paciente"]." ".@$nombreArray["apellido_materno_paciente"]." ".@$nombreArray["nombre_paciente"];
	}
	
	public function getPacienteEdad($id){
		$this->db->select("date(fecha_nacimiento_paciente)");
		$this->db->where("id_p",$id);
		$nacimiento = $this->db->get("pacientes");
		$array = $nacimiento->row_array();
		$fechaNacimiento =  $array["date(fecha_nacimiento_paciente)"];
		list($ano,$mes,$dia) = explode("-",$fechaNacimiento); 
		$ano_diferencia = date("Y") - $ano; 
		$mes_diferencia = date("m") - $mes; 
		$dia_diferencia = date("d") - $dia; 
		if ($dia_diferencia < 0 || $mes_diferencia < 0) 
		$ano_diferencia--; 
		return $ano_diferencia;
	}
	
	public function getPacienteSexo($id){
		$this->db->select("sexo_paciente");
		$this->db->where("id_p",$id);
		$nacimiento = $this->db->get("pacientes");
		$array = $nacimiento->row_array();
		return $array["sexo_paciente"];
	}
	
	public function getListaEnfermedades(){
		
		$arrayFinalEnfermedades = array();
		$this->db->select("nombre");
		$this->db->order_by("nombre", "asc"); 
		$enfermedades = $this->db->get("enfermedades");
		$array_enfermedades = $enfermedades->result_array();
		for($i=0;$i<count($array_enfermedades);$i++){
			$array_enfermedades[$i]["enfermedad"] = $array_enfermedades[$i]["nombre"];
		}
		
		return $array_enfermedades;
		
	}
	
}