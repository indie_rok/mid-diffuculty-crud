<?php

class DataModel extends CI_Model{

	public function __construct(){
		parent:: __construct();	
	}
	
	public function agregar($tabla,$data){
		$this->db->insert($tabla,$data);
	}
	
	public function eliminar($id){
		$this->db->delete($id);
	}
	
	public function editar(){
		
	}
	
	public function cargar(){
		
	}
}