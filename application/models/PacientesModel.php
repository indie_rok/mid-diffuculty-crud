<?php 
class PacientesModel extends CI_Model{

	public function __construct(){
		parent:: __construct();	
	}
		
	public function getPacientes($offset,$lim){
		$this->db->limit(20,$lim);
		$this->db->order_by("id_p", "desc"); 
		$pacientes = $this->db->get("pacientes");
		return $pacientes->result_array();	
	}
	
	public function  getNumeroPacientes(){
		$pacientes = $this->db->get("pacientes");
		return $pacientes->num_rows;	
	}
	
	public function insertarPaciente($data){
		$this->db->insert("pacientes",$data);	
	}
}