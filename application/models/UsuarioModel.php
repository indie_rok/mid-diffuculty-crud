<?php

	class UsuarioModel extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->load->model("DataModel");
	}
	
	public function login($data){
		if($this->usuarioExiste($data["nombre_usuario"])){
			if($this->claveCorrecta($data)){
				$this->asignarSessionData($data);
				return true;
			}
			else{
				$this->session->set_flashdata(array("Error" => "Password Incorrecto"));
				return false;
			}
		}
		else{
			$this->session->set_flashdata(array("Error" => "No existe usuario"));
			return false;
		}
	}
	
	public function usuarioExiste($usuario){
		$this->db->where("nombre_usuario",$usuario);
		$query = $this->db->get("usuarios");
		if($query->num_rows()==1){
			return true;	
		}
		else{
			return false;	
		}
	}
	
	public function claveCorrecta($data){		
		$this->db->where("nombre_usuario",$data["nombre_usuario"]);
		$this->db->where("clave_usuario",$data["clave_usuario"]);
		$query = $this->db->get("usuarios");
		
		if($query->num_rows()==1){
			return true;	
		}	
		else{
			return false;	
		}
	}
	
	private function getUserId($user){
		$this->db->select("id_u");
		$this->db->where("nombre_usuario",$user);	
		$userID = $this->db->get("usuarios");
		return $userID->row_array();
	}
	
	public function editar($data){
		try{
		$this->db->where("id_u",$data["id_u"]);
		$this->db->update("usuarios",$data);	
		
		} catch (Exception $e) {
  //alert the user then kill the process
  var_dump($e->getMessage());
}
	}
	
	private function asignarSessionData($data){
		
		$session = array (
			"id_u" => $this->getUserId($data["nombre_usuario"]),
			"nombre_usuario" => $data["nombre_usuario"],
			"logged_in" => 1,
		);
		
		$this->session->set_userdata($session);	
	}
	
	public function listar($nombre = null){
		$usuarios = $this->db->get("usuarios");
		return $usuarios->result_array();
	}
	
	public function getListaSucursales(){
		$this->db->select("clave_su,nombre_su");
		$sucursales = $this->db->get("sucursales");
		return $sucursales->result_array();
	}
	
	public function getListaEstados(){
		$this->db->select("cve_ent,nom_ent");
		$sucursales = $this->db->get("cat_entidades");
		return $sucursales->result_array();
	}
	
	public function getListaLocalidad($estado){
		
		$query = $this->db->query("SELECT DISTINCT cve_mun,nom_mun from cat_municipios WHERE cve_ent = ".$estado);

		return json_encode($query->result_array());
	}
	
	public function getListaMunicipio($estado,$municipio){
		
		$query = $this->db->query("SELECT DISTINCT localidad_nombre, localidad_cve
									 from cat_localidades
									 WHERE municipio_cve = '$municipio'
									 and entidad_cve= '$estado'
									 order by localidad_nombre asc"
								  );

		return json_encode($query->result_array());
	}
	
	public function getEscolaridad(){
		$escolaridad = $this->db->get("catalogo_escolaridad");
		return  $escolaridad->result_array();
	}
	
	public function getPuestos(){
		$this->db->select("clave_cp,nombre_cp");
		$escolaridad = $this->db->get("catalogo_puestos");
		return  $escolaridad->result_array();
	}
	
	public function getDepartamentos(){
		$this->db->select("id_d,nombre_d");
		$escolaridad = $this->db->get("departamentos");
		return  $escolaridad->result_array();
	}
	
	public function getAreasDepartamento($departamento){
		$query = $this->db->query("SELECT id_a,nombre_a from areas WHERE departamento_a = '$departamento' order by nombre_a asc");
		return json_encode($query->result_array());
	}
	
	public function insertarUsuario($data){
		$this->db->insert("usuarios",$data);	
	}
	
	public function getNumeroEmpleado(){
		return rand(9999,999999);	
	}
	
	public function getUserInfo($id){
		$this->db->where("id_u",$id);
		$usuario = $this->db->get("usuarios");
		return json_encode($usuario->row_array());
	}
}
