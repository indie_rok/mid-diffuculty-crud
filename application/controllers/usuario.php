<?php

class Usuario extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->userIsLoged = $this->session->userdata("logged_in") == 1;
		$this->load->model("UsuarioModel");	
		$this->load->helper('UsuariosForm');
	}
	
	public function index(){
		if (!$this->userIsLoged)
        { 
			$this->load->view("usuario/login");;
        }
		else{
			redirect("usuario/dashboard");	
		}
		
	}
	
	public function login(){
		if($this->UsuarioModel->login($this->input->post())){
			redirect("usuario/dashboard");
		}
		else{
			redirect("usuario/index");
		}
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect("usuario/index");
	}
	
	public function dashboard(){
		$this->load->view("usuario/dashboard");
			
	}
	
	public function listar(){
		$this->load->library('table');
		$data["usuarios"] = $this->UsuarioModel->listar();
		$data["headerTabla"] = array("NOMBRE","PUESTO","SUCURSAL","DEPARTAMENTO","USUARIO","ACTIVO","EDITAR");
		$data["listaSucursales"] = $this->UsuarioModel->getListaSucursales();
		$data["listaEstados"] = $this->UsuarioModel->getListaEstados();
		$data["escolaridad"] = $this->UsuarioModel->getEscolaridad();
		$data["puestos"] = $this->UsuarioModel->getPuestos();
		$data["departamentos"] = $this->UsuarioModel->getDepartamentos();
		$data["numeroEmpleado"] = $this->UsuarioModel->getNumeroEmpleado();
		$this->load->view("usuario/tabla",$data);
	}
	
	public function agregar(){
		if($this->input->server('REQUEST_METHOD') === 'POST'){
			$this->UsuarioModel->insertarUsuario($this->input->post());
			$this->session->set_flashdata(array("msj"=>"Usuario agregado con exito"));
			redirect("usuario/listar");
		}
		else{
			$data["listaSucursales"] = $this->UsuarioModel->getListaSucursales();
			$data["listaEstados"] = $this->UsuarioModel->getListaEstados();
			$data["escolaridad"] = $this->UsuarioModel->getEscolaridad();
			$data["puestos"] = $this->UsuarioModel->getPuestos();
			$data["departamentos"] = $this->UsuarioModel->getDepartamentos();
			$data["numeroEmpleado"] = $this->UsuarioModel->getNumeroEmpleado();
			$this->load->view("usuario/agregarUsuario",$data);
		}
	}
	
	public function ajaxLocalidad(){
		if($this->input->is_ajax_request())
		{
			$data = $this->input->post('estado');
			echo $this->UsuarioModel->getListaLocalidad($data);
			
		}
		else{
			redirect("usuario");	
		}
	}
	
	public function ajaxMunicipio(){
		if($this->input->is_ajax_request())
		{
			$estado = $this->input->post('estado');
			$localidad = $this->input->post('localidad');
			echo $this->UsuarioModel->getListaMunicipio($estado,$localidad);
		}
		else{
			redirect("usuario");	
		}
	}
	
	public function ajaxArea(){
		if($this->input->is_ajax_request())
		{
			$departamento = $this->input->post('departamento');
			echo $this->UsuarioModel->getAreasDepartamento($departamento);
		}
		else{
			redirect("usuario");	
		}
	}
	
	public function editar(){
		$this->UsuarioModel->editar($this->input->post());
		$this->session->set_flashdata(array("msj" => "Usuario Editado con exito"));
		redirect("usuario/listar");
	}
	
	public function ajaxUsuario(){
		echo $this->UsuarioModel->getUserInfo($this->input->post('id'));
	}
}