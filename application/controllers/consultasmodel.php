<?php 

class consultasmodel extends CI_Model{
	
	public function __construct(){
		parent:: __construct();		
	}
	
	public function getConsultasPendientes(){
		$consultas = $this->db->get("consultas");
		$array = $consultas->array_result();
		$username = getUserName($array["id_paciente_con"]);
	}
	
	private function getPacienteName($id){
		$this->db->select("nombre_paciente,apellido_paterno_paciente,apellido_materno_paciente");
		$paciente  = $this->db->get("pacientes");
		return $paciente->row_array();
	}
	
}