<?php 

class paciente extends CI_Controller{
	
	public function __construct(){
		parent::__construct();	
		$this->load->model("PacientesModel");
		$this->load->model("UsuarioModel");
	}
		
	public function listar($lim=null){
		
		$this->config->load("pagination");
		$this->load->library('pagination');
		
		$config['base_url'] = 'http://localhost/sigmanew/index.php/paciente/listar/';
		$config['total_rows'] = $this->PacientesModel->getNumeroPacientes();
		$config['per_page'] = 20;
		$this->pagination->initialize($config);
		
		if($lim==null){
			$pacientes = $this->PacientesModel->getPacientes($config['per_page'],0);
		}
		
		else{
			$pacientes = $this->PacientesModel->getPacientes($config['per_page'],$lim);
		}
		
		$data["headerTabla"] = array(
			"FOLIO",
			"NOMBRE",
			"EDAD",
			"VISITAS",
			"SEXO",
			"CIUDAD",
			"HISTORIA",
			"C"
		);
		
		$data["pacientes"] = $pacientes;
		$data["links"] = $this->pagination->create_links();
		
		$this->load->view("pacientes/tabla",$data);
	}
	
	public function agregar(){
		
		$this->load->helper("UsuariosForm_helper");
		
		if($this->input->server('REQUEST_METHOD') === 'POST'){
			$this->PacientesModel->insertarPaciente($this->input->post());
			$this->session->set_flashdata(array("msj"=>"Paciente agregado con exito"));
			redirect("paciente/listar");
		}
		else{
			$data["listaSucursales"] = $this->UsuarioModel->getListaSucursales();
			$data["listaEstados"] = $this->UsuarioModel->getListaEstados();
			$data["escolaridad"] = $this->UsuarioModel->getEscolaridad();
			$data["puestos"] = $this->UsuarioModel->getPuestos();
			$data["departamentos"] = $this->UsuarioModel->getDepartamentos();
			$data["numeroEmpleado"] = $this->UsuarioModel->getNumeroEmpleado();
			$this->load->view("pacientes/agregarPaciente",$data);
		}
	}
}