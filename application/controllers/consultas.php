<?php

class consultas extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model("consultasmodel");
	}
	
	public function pendientes(){
		$data["consultasPendientes"] = $this->consultasmodel->getConsultasPendientes();
		$data["headerTabla"] = array("REFERENCIA","PACIENTE","CONSULTA","URGENTE","ATENDER","ESTATUS");
		$this->load->view("consultas/consultasPendientes",$data);
	}
	
	public function atender($idConsulta){
		$idPaciente = $this->consultasmodel->getPacienteId($idConsulta);
		$data["edadPaciente"] = $this->consultasmodel->getPacienteEdad($idPaciente) ;
		$data["nombrePaciente"] = $this->consultasmodel->getPacienteName($idPaciente);
		$data["sexoPaciente"] = $this->consultasmodel->getPacienteSexo($idPaciente);
		$data["listaEnfermedades"] = $this->consultasmodel->getListaEnfermedades();
		
		$this->load->helper("UsuariosForm_helper");
		
		$this->load->view("consultas/formularioConsulta",$data);
	}
	
	public function agregar(){
		echo "Nueva Consulta";	
	}
}