<?php $this->load->view("template/header",array("titulo" => "Consultas Pendientes")); ?>

<div class="container contenedorTabla">

<div class="row">
<div class="col-xs-1">
<a href="<?php echo site_url("consultas/agregar")?>"><img src="<?php echo base_url() ?>assets/img/nvo_paciente1.png" title="Agregar Paciente"></a> 
</div>

<div class="col-xs-6 col-md-push-1">
    <input type="text" id="search" class="form-control" placeholder="Buscar..">
</div><!--/row-->
<div class="clearfix"></div>

<h1 style="color:green; background-color:#fff;"><?php echo $this->session->flashdata('msj'); ?></h1>

<table class="usuariosTabla table table-hover  table-striped table-condensed" style="background-color:#007BBE">
<tr class="tablaTitulo" >
<?php foreach($headerTabla as $row) { ?>
	<td><?php echo $row ?></td>
<?php 
}?>
</tr>

<?php 
for($i=0;$i<count($consultasPendientes);$i++){?>
<tr class="contenidoUsuarios ">
   <td><?php echo $consultasPendientes[$i]["referencia_con"]?></td>
   <td><?php echo $consultasPendientes[$i]["nombreCompleto"]?></td>
   <td><?php echo $consultasPendientes[$i]["area_con"]?></td>
   <td><?php echo $consultasPendientes[$i]["urge_atender_con"]?></td>
   <td><a href="atender/<?php echo $consultasPendientes[$i]["id_con"]?>">Atender</a></td>
   <td><?php echo $consultasPendientes[$i]["estatus_con"]?></td>
</tr>
<?php }?>
</table>


	<div id="dialogoUsuario" style="display:none">
    
    <?php echo form_open("usuario/editar") ?>
    
    
    <input type="submit" value="Editar" class="btn btn-success pull-right">
    <input type="hidden" name="id_u">

<script>

$('#search').on('keyup', function() {
    var rex = new RegExp($(this).val(), 'i');
    $('.usuariosTabla tr.contenidoUsuarios').hide();
        $('.usuariosTabla tr.contenidoUsuarios').filter(function() {
            return rex.test($(this).text());
        }).show();
    });
</script>

<?php $this->load->view("template/footer"); ?>