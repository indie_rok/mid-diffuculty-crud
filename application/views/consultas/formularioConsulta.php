<?php $this->load->view("template/header",array("titulo" => "Atender Paciente")); ?>

<?php form_open("consultas/crearConsulta"); ?>

<div class="container">

<ul class="nav nav-tabs agregar-usuario center-block">
    <li class="active"><a data-toggle="tab" href="#general" style="background-color:#ED7B08">General</a></li>
    <li><a data-toggle="tab" href="#direccion" style="background-color:#A2BD30">Historia Clinica</a></li>
    <li><a data-toggle="tab" href="#contacto" style="background-color:#C9C9C9">Dictamen</a></li>
    <li><a data-toggle="tab" href="#adicionales" style="background-color:#664686">Receta</a></li>
</ul>

    <div class="tab-content">
      <div id="general" class="tab-pane fade in active">
            
        <h1>Datos del paciente</h1>
        
        <div class="form-group">
        <label>Nombre</label>
        <input type="text" class="form-control" value="<?php echo $nombrePaciente ?>" disabled>
        </div>
        
        <div class="form-group">
        <label>Edad</label>
        <input type="text" class="form-control" value="<?php echo $edadPaciente?>">
        </div>
        
        
        <div class="form-group">
        <label for="">Sexo</label>
        
        <input type="radio" name="sexo_paciente" value="H" disabled <?php if($sexoPaciente == 1){echo "checked";}?>>Mujer
        <input type="radio" name="sexo_paciente" value="M" disabled <?php if($sexoPaciente == 2){echo "checked";}?>>Hombre
        </div> 
        
        <div class="form-group">
        <label>Peso</label>
        <input type="text" class="form-control">kg
        </div>
        
        <div class="form-group">
        <label>Talla</label>
        <input type="text" class="form-control">mts
        </div>
        
        <div class="form-group">
        <label>IMC</label>
        <input type="text" value="imc" disabled class="form-control">kg/m^2
        </div>
        
        <div class="form-group">
        <label>FR</label>
        <input type="text" class="form-control">X Min	
        </div>
        
        <div class="form-group">
        <label>FC</label>
        <input type="text" class="form-control">X Min
        </div>	
        
        <div class="form-group">
        <label>Temp</label>
        <input type="text" class="form-control">°C
        </div>
        
        <div class="form-group form-inline">
        <label>T/A</label>
        <input type="text" class="form-control"> / <input type="text" class="form-control">
        </div>
        
        <div class="form-group">
        <label>Fecha Ingreso</label>
        <input type="date" class="form-control">
        </div>
            
        <div class="form-group">
        <textarea class="form-control" placeholder="Motivo de Consulta.."></textarea>		
        </div>  
                
        <div class="form-group">	  
        <textarea class="form-control" placeholder="Notas.."></textarea>
        </div> 
        
       </div><!--/general-->
            
      <div id="direccion" class="tab-pane fade">
        <h1>HISTORIA CLÍNICA</h1>
        
        <a href="#">AHF</a>		<a href="#">APP	</a>
        <a href="#">APNP</a>	<a href="#">AGO	</a>
        
        <div id="ahf">
        	<h1>Antecedentes Heredo Familiares</h1>
            
            <label>Padre</label>
            <select>
           	 <option>Vivo</option>
           	 <option>Finado</option>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <br>
            
            <label>Madre</label>
            <select>
           	 <option>Vivo</option>
           	 <option>Finado</option>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <br>
            
            <label>Conyugue</label>
            <select>
           	 <option>Vivo</option>
           	 <option>Finado</option>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
			 <?php 
             $arrayHermanosOpciones = array();
             for($i=0;$i<15;$i++){
                 $arrayHermanosOpciones[$i]["option"] = $i;
                 $arrayHermanosOpciones[$i]["value"] = $i;
             }
             ?>
             
             
              <br>
            
            <label>Madre</label>
            <select>
           	 <option>Vivo</option>
           	 <option>Finado</option>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <br>
            
            <label>Hermanos</label>
            
            <select>
            <?php dropdownDinamico($arrayHermanosOpciones,false);?>
          	</select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <br>	
            
            <label>Hijos</label>
            
            <select>
            <?php dropdownDinamico($arrayHermanosOpciones,false);?>
          	</select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedade);?>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <select>
            <?php dropdownDinamico($listaEnfermedades);?>
            </select>
            
            <textarea placeholder="Notas"></textarea>
            
         </div>
         
         <a href="#">Ir a la Historia Clínica Completa</a>
         
      </div><!--/contacto-->
               
      <div id="contacto" class="tab-pane fade">
        
        <h1>DICTAMEN MÉDICO</h1>
            
        <a href="#">Agregar Dictamen Médico</a>
        
      </div><!--/contacto-->    
                   
      <div id="adicionales" class="tab-pane fade">
        
        <h1>RECETA MÉDICA</h1>
        <a href="#">Agregar Medicamento</a>
        
      </div><!--/adicionales-->
     
     </div><!--/tab content-->
     
 </div><!--/container-->

<?php form_close() ?>

<?php $this->load->view("template/footer"); ?>