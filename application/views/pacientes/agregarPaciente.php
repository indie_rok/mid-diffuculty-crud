<?php $this->load->view("template/header",array("paciente")); ?>

<div class="container push-bottom agregarUsuario">

<a href="javascript:history.back()" class="btn btn-danger pull-right">Cancelar</a>

<h1>Agregar paciente</h1>

<div class="clearfix"></div>

<?php echo form_open("paciente/agregar",array("class" => "form","id"=>"formUsuario")) ?>

<ul class="nav nav-tabs agregar-usuario center-block">
    <li class="active"><a data-toggle="tab" href="#general" style="background-color:#ED7B08">General</a></li>
    <li><a data-toggle="tab" href="#direccion" style="background-color:#A2BD30">Direccion</a></li>
    <li><a data-toggle="tab" href="#contacto" style="background-color:#C9C9C9">Contacto</a></li>
    <li><a data-toggle="tab" href="#adicionales" style="background-color:#664686">Adicionales</a></li>
</ul>

<div class="tab-content">
    <div id="general" class="tab-pane fade in active">
        
        <div class="row">
            <div class="col-xs-4">
            
            	<img src="http://dummyimage.com/200x140" class="center-block">
                <a href="#"  class="center-block text-center">Agregar Imagen</a>               
            </div><!--/colxs4-->
             
             <div class="col-xs-4">
             	
                <div class="form-group">
                    <label for="">Nombre(s)*</label>
                    <input type="text" name="nombre_paciente" class="form-control">
                </div>
             
                <div class="form-group">
                    <label for="">A.Paterno**</label>
                    <input type="text" name="apellido_paterno_paciente" class="form-control">
                </div>
             </div><!--/colxs4-->
             
             <div class="col-xs-4">
                <div class="form-group">
                    <label for="">A.Materno</label>
                    <input type="text" name="apellido_materno_paciente" class="form-control">
                </div>
             </div><!--/colxs4-->
         </div><!--/row-->
         
        <div class="row">
            <div class="col-xs-4">
                <div class="form-group">
                    <label for="">Sexo</label>
                    <div class="radio">
                      <label>
                        <input type="radio" name="sexo_paciente"  value="2">
                        Masculino
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="sexo_paciente"  value="1">
                        Femenino
                      </label>
                    </div>
                </div>
            </div><!--/col xs 2-->
            
            <div class="col-xs-4">
                <div class="form-group">
                    <label for="">Nacionalidad</label>
                    <div class="radio">
                      <label>
                        <input type="radio" name="nacionalidad_paciente" value="mexicano" id="nacionalidad_paciente"
                        onClick="javascript:document.getElementById('nacionalidadExtra').style.display='none'">
                        Mexicano
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="nacionalidad_paciente"   id="radioExtranjero" value="extranjero" 
                        onClick="javascript:document.getElementById('nacionalidadExtra').style.display='block'">
                        Extranjero
                      </label>
                    </div>
                    
                    <div class="form-group" id="nacionalidadExtra" style="display:none">
                  	  <label for="nacionalidad">De que país?</label>
                  	  <input type="text" class="form-control" id="nacionalidadText" name="pais_domicilio_pa">
                    </div>
                    
                </div>
            </div><!--/col xs 2-->
            
            <div class="col-xs-4">
                <div class="form-group">
                    <label for="">Fecha de Nacimiento*</label>
                    <input type="date" name="fecha_nacimiento_paciente" class="form-control">
                </div>
           </div><!--/col xs 2-->
        
         
        </div><!--/row-->
        
       <div class="row">
       
       <div class="col-xs-4">
            <div class="form-group">
            	<label for="">CURP</label>
            	<input type="text" name="curp_paciente" class="form-control">
       		 </div>
         </div><!--/col xs 2-->
       
       	<div class="col-xs-4">
        
            <div class="form-group">
                <label for="">RFC</label>
                <input type="text" name="rfc_paciente" class="form-control">
            </div>
        </div><!--/col xs 3-->
        
        <div class="col-xs-4">
            <div class="form-group">
                <label for="">Homoclave</label>
                <input type="text" name="homoclave_paciente" class="form-control">
            </div>
         </div><!--/col xs 3-->
         
        </div><!--row-->
        
             
        <div class="form-group">
            <label for="">Notas</label>
            <textarea class="form-control" name="notas_paciente" rows="2"></textarea> 
        </div>
    </div><!--/general-->
    
    <div id="direccion" class="tab-pane fade">
        <!--SECOND SCREEN-->
            
        <div class="row">
            
          <div class="col-xs-4">
            <div class="form-group">
                <label for="">Estado*</label>
                <select class="form-control" name="estado_domicilio_pa" id="dropdownEstados">
                    <?php dropdownDinamico($listaEstados); ?>
                </select>
            </div>	
           </div><!--/colxs4-->
          
         <div class="col-xs-4">   
            <div class="form-group">
                <label for="">Localidad</label>
                <select class="form-control" name="localidad_domicilio_pa"  id="dropdownLocalidad">
                    <!--ajax response-->
                </select>
            </div>	
          </div><!--/colxs4--> 
          
           <div class="col-xs-4">
            <div class="form-group">
                <label for="">Municipio*</label>
                <select class="form-control" name="municipio_domicilio_pa" id="dropdownMunicipios">
                	<!--ajax response-->
                </select>
            </div>	
          </div><!--/colxs4-->
           
        </div><!--/row-->
        
        <div class="row">
        
          <div class="col-xs-4">
          	
            <div class="form-group">
                <label for="">Calle</label>
                <input type="text" name="calle_domicilio_pa" class="form-control">
            </div>	
            
          </div><!--/col-xs-4-->
          
          <div class="col-xs-3">
          	<div class="form-group">
                <label for="">Colonia</label>
                <input type="text" name="colonia_domicilio_pa" class="form-control">
            </div>  
          </div><!--/col-xs-3-->  
          
          <div class="col-xs-3"> 
          	<div class="form-group">
                <label for="">C.P.</label>
                <input type="text" name="codigo_postal_domicilio_pa" class="form-control">
            </div>
            
          </div><!--/col-xs-3-->  
          
          <div class="col-xs-1">  
            <div class="form-group">
                <label for="">NumInt</label>
                <input type="text" name="numero_int_domicilio_pa" class="form-control">
            </div>
          </div><!--/col md 1-->
            
          <div class="col-xs-1">   
            <div class="form-group">
                <label for="">NumExt</label>
                <input type="text" name="numero_ext_domicilio_pa" class="form-control">
            </div>
          </div><!--/col md 1-->
        
        </div><!--/row-->
          
       </div><!--/contacto-->
       
    <div id="contacto" class="tab-pane fade">
           
        <div class="row">
        
           	<div class="col-xs-4">
                <div class="form-group">
                    <label for="">Teléfono Celular</label>
                    <input type="text" name="telefono_celular_paciente" class="form-control">
                </div>	
            </div><!--/col-xs-4-->
            
            <div class="col-xs-4">     
                <div class="form-group">
                    <label for="">Teléfono Partícular</label>
                    <input type="text" name="telefono_particular_paciente" class="form-control">
                </div>	
            </div><!--/col-xs-4-->   
             
             <div class="col-xs-4">      
                <div class="form-group">
                    <label for="">Teléfono Trabajo</label>
                    <input type="text" name="telefono_trabajo_paciente" class="form-control">
                </div>	
              </div><!--/col-xs-4-->
              
            </div><!--/row-->
             
            <div class="row">
            
              <div class="col-xs-4">    
                <div class="form-group">
                    <label for="">Teléfono Emergencia</label>
                    <input type="text" name="telefono_emergencia_paciente" class="form-control">
                </div>	
              </div><!--/col-xs-4-->
              
              <div class="col-xs-4">   
                <div class="form-group">
                    <label for="">Avisar a</label>
                    <input type="text" name="avisar_a" class="form-control">
                </div>	
              </div> <!--/col xs-4-->
              
             <div class="col-xs-4">   
                <div class="form-group">
                    <label for="">Email	</label>
                    <input type="text" name="" class="form-control">
                </div>
              </div> <!--/col xs-4-->  
              
            </div><!--row-->
            
        </div><!--/contacto-->    
           
    <div id="adicionales" class="tab-pane fade">
      <div class="row">
      
      	<div class="col-xs-6">
           <div class="form-group">
                 <label for="">Número Paciente</label>
                 <input type="text" name="numero_empleado" class="form-control" disabled value="<?php echo $numeroEmpleado ?>">
            </div>
         </div><!--/col-md-4-->
         
         <div class="col-xs-6">   
             <div class="form-group">
                    <label for="">Escolaridad</label>
                    <select class="form-control" name="escolaridad_paciente">
                        
                       <?php dropdownDinamico($escolaridad); ?>
                        
                    </select>
                </div>
           </div><!--/col-md-4-->
           
          
      </div><!--/row-->    
       
       <div class="row">  
       	
         
        <div class="col-xs-4"> 
            <div class="form-group">
                <label for="">Tipo Beneficiario*</label>
                <input type="text" name="tipo_beneficiario_paciente" class="form-control">
            </div>
         </div><!--/col cs 3-->
         
         <div class="col-xs-4">   
            <div class="form-group">
                <label for="">Clave de dependencia*</label>
                <input type="text" name="clave_dependencia_paciente" class="form-control">
            </div>
         </div><!--/col cs 3-->  
            
         <div class="col-xs-4">     
            <div class="form-group">
                <label for="">Clave de programa*</label>
                <input  type="text" name="clave_programa_paciente" class="form-control">
            </div>
         </div><!--/col cs 3--> 

       	</div><!--/row-->
    </div><!--/adicionales-->
</div><!--container-->


<input type="submit" class="btn btn-default" value="Agregar">

<?php echo form_close();?>

</div><!--/container-->

<script>

$("#formUsuario").on("submit",function(){
	if($("#radioExtranjero").is(":checked")){
		$("#nacionalidad_paciente").val($("#nacionalidadText").val()) ;
	}
});

</script>
<script>
$("#dropdownEstados").change(function(){
	$("#dropdownLocalidad").empty();
	$("#dropdownMunicipios").empty();
	var estado = $( "#dropdownEstados" ).find(":selected").val();
	$.ajax({
         type: "POST",
         url: '<?php echo site_url("usuario/ajaxLocalidad")?>', 
         data: {estado:estado},
         dataType: "text",  
         cache:false,
         success: 
              function(data){
				
				var optionValues = $.parseJSON(data);
				
				var opts;
				
				for (var i = 0; i < optionValues.length; i++) {
					opts += "<option value='" + optionValues[i].cve_mun + "'>" + optionValues[i].nom_mun + "</option>";
				}
				$("#dropdownLocalidad").append(opts);	
				
              }
          });
     return false;
}); 

$("#dropdownLocalidad").change(function(){
	$("#dropdownMunicipios").empty();
	var estado = $( "#dropdownEstados" ).find(":selected").val();
	var localidad = $("#dropdownLocalidad").find(":selected").val();
		
		$.ajax({
			 type: "POST",
			 url: '<?php echo site_url("usuario/ajaxMunicipio")?>', 
			 data: {estado: estado,localidad:localidad},
			 dataType: "text",
			 cache:false,
			 success: 
				  function(data){
					
					var optionValues = $.parseJSON(data);
					var opts;
					
					for (var i = 0; i < optionValues.length; i++) {
						opts += "<option value='" + optionValues[i].localidad_cve + "'>" + optionValues[i].localidad_nombre + "</option>";
					}
					$("#dropdownMunicipios").append(opts);	
					
				  }
			  });
		 return false;
}); 


$("#dropdownDepartamento").change(function(){
	$("#dropdownArea").empty();
	var departamento = $( "#dropdownDepartamento" ).find(":selected").val();
	$.ajax({
         type: "POST",
         url: '<?php echo site_url("usuario/ajaxArea")?>', 
         data: {departamento:departamento},
         dataType: "text",  
         cache:false,
         success: 
              function(data){
				
				var optionValues = $.parseJSON(data);
				
				var opts;
				
				for (var i = 0; i < optionValues.length; i++) {
					opts += "<option value='" + optionValues[i].id_a + "'>" + optionValues[i].nombre_a + "</option>";
				}
				$("#dropdownArea").append(opts);	
				
              }
          });
     return false;
}); 


</script>

<?php $this->load->view("template/footer"); ?>