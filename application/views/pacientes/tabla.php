<?php $this->load->view("template/header",array("titulo" => "Pacientes")); ?>


<div class="container contenedorTabla">

<div class="row">
<div class="col-xs-1">
<a href="<?php echo site_url("paciente/agregar")?>"><img src="<?php echo base_url() ?>assets/img/nvo_paciente1.png" title="Agregar Paciente"></a> 
</div>

<div class="col-xs-6 col-md-push-1 ">
    <input type="text" id="search" class="form-control" placeholder="Buscar..">
</div><!--/row-->
<div class="clearfix"></div>
<h1 style="color:green"><?php echo $this->session->flashdata('msj'); ?></h1>

<table class="pacientesTabla table table-hover  table-striped table-condensed" style="background-color:#007BBE">
    <tr class="tablaTitulo" >
    <?php foreach($headerTabla as $row) { ?>
        <td><?php echo $row ?></td>
    <?php 
    }?>
    </tr>

	<?php 
    for($i=0;$i<count($pacientes);$i++){?>
    <tr class="contenidoPacientes ">
    
       <td>CAD  <?php echo $pacientes[$i]["id_p"]?></td>
       <td><?php echo $pacientes[$i]["nombre_paciente"]." " .
             $pacientes[$i]["apellido_paterno_paciente"]. " " . 
             $pacientes[$i]["apellido_materno_paciente"] ?>
       </td>
       <td><?php echo $pacientes[$i]["fecha_nacimiento_paciente"]?></td>
       <td><?php echo $pacientes[$i]["visitas"]?></td>
       <td><?php if($pacientes[$i]["sexo_paciente"]==1){echo "M";} else{echo "H";}?></td>
       <td><?php echo $pacientes[$i]["ciudad_domicilio_pa"]?></td>
       <td><a href="#">Accesar</a></td>
       <td><a href="#"><img src="<?php echo base_url() ?>assets/img/ver_modificaciones.png"></a></td>
    </tr>
    <?php }?>
</table>

	<?php echo $links ?>

	<div id="dialogoUsuario" style="display:none">
    
    <?php echo form_open("usuario/editar") ?>
    
    <input type="submit" value="Editar" class="btn btn-success pull-right">
    
    <div class="clearfix"></div>
    
     <label for="">ID*</label>
     <input type="text" name="id_u" class="form-control">
     <label for="">Nombre(s)*</label>
     <input type="text" name="nombre_usuario" class="form-control">
     <label for="">A.Paterno**</label>
     <input type="text" name="apellido_paterno_usuario" class="form-control">
     <label for="">A.Materno</label>
     <input type="text" name="apellido_materno_usuario" class="form-control">
     <label for="">Fecha de Nacimiento*</label>
     <input type="date" name="fecha_nacimiento_usuario" class="form-control">
     <label for="">Tipo Usuario*	</label>
     <select name="tipo_usuario" id="tipoUsuario" class="form-control">
           <option value="0">-SELECCIONAR-</option>
           <option value="1">ADMINISTRADOR</option>
           <option value="2">JEFE DE DEPARTAMENTO</option>
           <option value="3">RECEPCIONISTA</option>
           <option value="4">VISITANTE</option>
     </select>
                    
                 	<label for="">Sucursal</label>
                    <select class="form-control" name="sucursal_usuario">
                    <?php dropdownDinamico($listaSucursales);?>
                    </select>
                    
            	<label for="">CURP</label>
            	<input type="text" name="curp_usuario" class="form-control">
                
                <label for="">RFC</label>
                <input type="text" name="rfc_usuario" class="form-control">
                
                <label for="">Homoclave</label>
                <input type="text" name="homoclave_usuario" class="form-control">
                
                <label for="">Clave Usuario*</label>
                <input type="password" name="clave_usuario" class="form-control">
                
            <label for="">Notas</label>
            <textarea class="form-control" name="notas_usuario" rows="2"></textarea> 
            
            
                <label for="">Calle</label>
                <input type="text" name="calle_domicilio_us" class="form-control">
                
                <label for="">Colonia</label>
                <input type="text" name="colonia_domicilio_us" class="form-control">
                
                <label for="">C.P.</label>
                <input type="text" name="codigo_postal_domicilio_us" class="form-control">
                
                <label for="">NumInt</label>
                <input type="text" name="numero_int_domicilio_us" class="form-control">
                
                <label for="">NumExt</label>
                <input type="text" name="numero_ext_domicilio_us" class="form-control">
                
                    <label for="">Teléfono Celular</label>
                    <input type="text" name="telefono_celular_usuario" class="form-control">
                    
                    <label for="">Teléfono Partícular</label>
                    <input type="text" name="telefono_particular_usuario" class="form-control">
                    
                    <label for="">Teléfono Trabajo</label>
                    <input type="text" name="telefono_trabajo_usuario" class="form-control">
                    
                
                    <label for="">Teléfono Emergencia</label>
                    <input type="text" name="telefono_emergencia_usuario" class="form-control">
                    
                    <label for="">Avisar a</label>
                    <input type="text" name="avisar_a_usuario" class="form-control">
                    
                    <label for="">Email	</label>
                    <input type="text" name="" class="form-control">
                    
                 <label for="">Número Empleado</label>
                 <input type="text" name="numero_empleado" class="form-control" disabled>
                 
               
               
		
        <?php echo form_close() ?>
        
</div><!--/container-->
<script>

$(".detallesUsuario").click(ajax); 

function ajax(){
	
	var id  = $(this).attr("id");
	
	$.ajax({
         type: "POST",
         url: '<?php echo site_url("usuario/ajaxUsuario/")?>', 
         data: {id:id},
         cache:false,
         success: 
              function(data){
				  	var usuario = $.parseJSON(data);
					
					$("[name=id_u]").val(usuario.id_u);
					
					$("[name=nombre_usuario]").val(usuario.nombre_usuario);
					$("[name=apellido_paterno_usuario]").val(usuario.apellido_paterno_usuario);
					$("[name=apellido_materno_usuario]").val(usuario.apellido_materno_usuario);
					$("[name=clave_usuario]").val(usuario.clave_usuario);
					$("[name=cedula_usuario]").val(usuario.cedula_usuario);
					$("[name=curp_usuario]").val(usuario.curp_usuario);
										
					$("#dialogoUsuario").dialog({
						title:usuario.nombre_usuario+" | Perfil",
						minHeight:600,
						minWidth:1000,
					});
					
              }
          });
     return false;	
}

$('#search').on('keyup', function() {
    var rex = new RegExp($(this).val(), 'i');
    $('.pacientesTabla tr.contenidoPacientes').hide();
        $('.pacientesTabla tr.contenidoPacientes').filter(function() {
            return rex.test($(this).text());
        }).show();
    });
</script>

<?php $this->load->view("template/footer"); ?>