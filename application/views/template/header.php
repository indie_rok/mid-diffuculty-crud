<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php if(!empty($titulo))
		{
			echo $titulo;
		}
		else{
			 echo "Sigma";
		} ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/main.css">
        
        <!--bootstrap-->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-theme.min.css">
        
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">
        
        <!--ubuntu font-->
        <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500' rel='stylesheet' type='text/css'>
        
        <!--jQuery & jquey UI-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/themes/dark-hive/jquery-ui.css" />
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
        
        <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>           
        <script src="<?php echo base_url()?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
    
    <?php if($this->session->userdata("logged_in" ) ==1){ ?>
    
   <nav class="navbar navbar-default spc-40px">
   
    <div class="col-xs-12 current-page">
    	<div class="container">
       	 <img src="<?php echo base_url()?>assets/img/icono paciente.png" height="25px" width="17px" style="margin-right:10px">
			<span id="tituloMenu" onClick="subMenu()"><?php echo @$titulo ?></span>
            <div class="pull-right">Hola <?php echo $this->session->userdata("nombre_usuario"); ?>
            <a href="<?php echo site_url("usuario/logout")?>">Logout</a>
            </div>
        </div>
    </div>
	<div class="container menuTop">
      <ul class="nav navbar-nav text-center" id="subMenu" style="display:none">
         <li><a href="javascript:history.back(-1);"><img src="<?php echo base_url()?>assets/img/iconos_menu/back.png"></a></li>
         <li><a href="<?php echo site_url("usuario/listar"); ?>"><img src="<?php echo base_url()?>assets/img/iconos_menu/usuarios.png"></a></li>
         <li><a href="<?php echo site_url("consultas/pendientes"); ?>"><img src="<?php echo base_url()?>assets/img/iconos_menu/consultas.png"></a></li>
         <li><a href="#"><img src="<?php echo base_url()?>assets/img/iconos_menu/diagnostico.png"></a></li>
         <li><a href="#"><img src="<?php echo base_url()?>assets/img/iconos_menu/servicios.png"></a></li>
         <li><a href="#"><img src="<?php echo base_url()?>assets/img/iconos_menu/farmacia.png"></a></li>
         <li><a href="#"><img src="<?php echo base_url()?>assets/img/iconos_menu/admon.png"></a></li>
         <li><a href="#"><img src="<?php echo base_url()?>assets/img/iconos_menu/caja.png"></a></li>
         <li><a href="#"><img src="<?php echo base_url()?>assets/img/iconos_menu/agenda.png"></a></li>
         <li><a href="#"><img src="<?php echo base_url()?>assets/img/iconos_menu/inventario.png"></a></li>
         <li><a href="<?php echo site_url("usuario/dashboard"); ?>"><img src="<?php echo base_url()?>assets/img/iconos_menu/inicio.png"></a></li>
      </ul>
      
    </div><!--/container-->
	</nav>
    <div class="clearfix"></div>
    <?php } else{echo"no";}?>
    
    <script>
	function subMenu(){
		
		var submenu = document.getElementById("subMenu");
		var submenuStatus = submenu.style.display;
		
		if(submenuStatus == "none"){
			submenu.style.display="block";	
		}
		else{
			submenu.style.display="none";
		}
	}
	</script>