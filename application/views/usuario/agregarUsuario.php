<?php $this->load->view("template/header"); ?>

<div class="container push-bottom agregarUsuario">

<a href="javascript:history.back()" class="btn btn-danger pull-right">Cancelar</a>

<h1>Agregar usuario</h1>
<?php echo form_open("usuario/agregar",array("class" => "form","id"=>"formUsuario")) ?>

<ul class="nav nav-tabs agregar-usuario center-block">
    <li class="active"><a data-toggle="tab" href="#general" style="background-color:#ED7B08">General</a></li>
    <li><a data-toggle="tab" href="#direccion" style="background-color:#A2BD30">Direccion</a></li>
    <li><a data-toggle="tab" href="#contacto" style="background-color:#C9C9C9">Contacto</a></li>
    <li><a data-toggle="tab" href="#adicionales" style="background-color:#664686">Adicionales</a></li>
</ul>

<div class="tab-content">
    <div id="general" class="tab-pane fade in active">
        
        <div class="row">
            <div class="col-xs-4">
                <div class="form-group">
                    <label for="">Nombre(s)*</label>
                    <input type="text" name="nombre_usuario" class="form-control">
                </div>
             </div><!--/colxs4-->
             
             <div class="col-xs-4">
                <div class="form-group">
                    <label for="">A.Paterno**</label>
                    <input type="text" name="apellido_paterno_usuario" class="form-control">
                </div>
             </div><!--/colxs4-->
             
             <div class="col-xs-4">
                <div class="form-group">
                    <label for="">A.Materno</label>
                    <input type="text" name="apellido_materno_usuario" class="form-control">
                </div>
             </div><!--/colxs4-->
         </div><!--/row-->
         
        <div class="row">
            <div class="col-xs-2">
                <div class="form-group">
                    <label for="">Sexo</label>
                    <div class="radio">
                      <label>
                        <input type="radio" name="sexo_usuario"  value="2">
                        Masculino
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="sexo_usuario"  value="1">
                        Femenino
                      </label>
                    </div>
                </div>
            </div><!--/col xs 2-->
            
            <div class="col-xs-2">
                <div class="form-group">
                    <label for="">Nacionalidad</label>
                    <div class="radio">
                      <label>
                        <input type="radio" name="nacionalidad_usuario" value="mexicano" id="nacionalidad_usuario"
                        onClick="javascript:document.getElementById('nacionalidadExtra').style.display='none'">
                        Mexicano
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="nacionalidad_usuario"   id="radioExtranjero" value="extranjero" 
                        onClick="javascript:document.getElementById('nacionalidadExtra').style.display='block'">
                        Extranjero
                      </label>
                    </div>
                    
                    <div class="form-group" id="nacionalidadExtra" style="display:none">
                  	  <label for="nacionalidad">De que país?</label>
                  	  <input type="text" class="form-control" id="nacionalidadText" name="pais_domicilio_us">
                    </div>
                    
                </div>
            </div><!--/col xs 2-->
            
            <div class="col-xs-2">
                <div class="form-group">
                    <label for="">Fecha de Nacimiento*</label>
                    <input type="date" name="fecha_nacimiento_usuario" class="form-control">
                </div>
           </div><!--/col xs 2-->
            
            <div class="col-xs-2">    
                <div class="form-group">
                    <label for="">Tipo Usuario*	</label>
                    <select name="tipo_usuario" id="tipoUsuario" class="form-control">
                      <option value="0">-SELECCIONAR-</option>
                      <option value="1">ADMINISTRADOR</option>
                      <option value="2">JEFE DE DEPARTAMENTO</option>
                      <option value="3">RECEPCIONISTA</option>
                      <option value="4">VISITANTE</option>
                    </select>
                </div>
            </div><!--/col xs 2-->
            
            <div class="col-xs-4">
            	<div class="form-group">
                 	<label for="">Sucursal</label>
                    <select class="form-control" name="sucursal_usuario">
                    <?php dropdownDinamico($listaSucursales);?>
                    </select>
                   
                    
            	 </div>
                 
                 <div class="checkbox form-group">
                  <label>
                    <input type="checkbox" checked name="activo_usuario">
                    Usuario Activo
                  </label>
        		</div>
                 
            </div><!--/col xs 2-->
        
         
        </div><!--/row-->
        
       <div class="row">
       
       <div class="col-xs-2">
            <div class="form-group">
            	<label for="">CURP</label>
            	<input type="text" name="curp_usuario" class="form-control">
       		 </div>
         </div><!--/col xs 2-->
       
       	<div class="col-xs-2">
        
            <div class="form-group">
                <label for="">RFC</label>
                <input type="text" name="rfc_usuario" class="form-control">
            </div>
        </div><!--/col xs 3-->
        
        <div class="col-xs-2">
            <div class="form-group">
                <label for="">Homoclave</label>
                <input type="text" name="homoclave_usuario" class="form-control">
            </div>
         </div><!--/col xs 3-->
         
         <div class="col-xs-3">   
            <div class="form-group">
                <label for="">Usuario*</label>
                <input type="text" name="usuario_usuario" class="form-control">
            </div>
         </div><!--/col xs 3-->
         
         <div class="col-xs-3">   
            <div class="form-group">
                <label for="">Clave Usuario*</label>
                <input type="password" name="clave_usuario" class="form-control">
            </div>
         </div><!--/col xs 3-->
         
        </div><!--row-->
        
             
        <div class="form-group">
            <label for="">Notas</label>
            <textarea class="form-control" name="notas_usuario" rows="2"></textarea> 
        </div>
    </div><!--/general-->
    
    <div id="direccion" class="tab-pane fade">
        <!--SECOND SCREEN-->
            
        <div class="row">
            
          <div class="col-xs-4">
            <div class="form-group">
                <label for="">Estado*</label>
                <select class="form-control" name="estado_domicilio_us" id="dropdownEstados">
                    <?php dropdownDinamico($listaEstados); ?>
                </select>
            </div>	
           </div><!--/colxs4-->
          
         <div class="col-xs-4">   
            <div class="form-group">
                <label for="">Localidad</label>
                <select class="form-control" name="localidad_domicilio_us"  id="dropdownLocalidad">
                    <!--ajax response-->
                </select>
            </div>	
          </div><!--/colxs4--> 
          
           <div class="col-xs-4">
            <div class="form-group">
                <label for="">Municipio*</label>
                <select class="form-control" name="municipio_domicilio_us" id="dropdownMunicipios">
                	<!--ajax response-->
                </select>
            </div>	
          </div><!--/colxs4-->
           
        </div><!--/row-->
        
        <div class="row">
        
          <div class="col-xs-4">
          	
            <div class="form-group">
                <label for="">Calle</label>
                <input type="text" name="calle_domicilio_us" class="form-control">
            </div>	
            
          </div><!--/col-xs-4-->
          
          <div class="col-xs-3">
          	<div class="form-group">
                <label for="">Colonia</label>
                <input type="text" name="colonia_domicilio_us" class="form-control">
            </div>  
          </div><!--/col-xs-3-->  
          
          <div class="col-xs-3"> 
          	<div class="form-group">
                <label for="">C.P.</label>
                <input type="text" name="codigo_postal_domicilio_us" class="form-control">
            </div>
            
          </div><!--/col-xs-3-->  
          
          <div class="col-xs-1">  
            <div class="form-group">
                <label for="">NumInt</label>
                <input type="text" name="numero_int_domicilio_us" class="form-control">
            </div>
          </div><!--/col md 1-->
            
          <div class="col-xs-1">   
            <div class="form-group">
                <label for="">NumExt</label>
                <input type="text" name="numero_ext_domicilio_us" class="form-control">
            </div>
          </div><!--/col md 1-->
        
        </div><!--/row-->
          
       </div><!--/contacto-->
       
    <div id="contacto" class="tab-pane fade">
           
        <div class="row">
        
           	<div class="col-xs-4">
                <div class="form-group">
                    <label for="">Teléfono Celular</label>
                    <input type="text" name="telefono_celular_usuario" class="form-control">
                </div>	
            </div><!--/col-xs-4-->
            
            <div class="col-xs-4">     
                <div class="form-group">
                    <label for="">Teléfono Partícular</label>
                    <input type="text" name="telefono_particular_usuario" class="form-control">
                </div>	
            </div><!--/col-xs-4-->   
             
             <div class="col-xs-4">      
                <div class="form-group">
                    <label for="">Teléfono Trabajo</label>
                    <input type="text" name="telefono_trabajo_usuario" class="form-control">
                </div>	
              </div><!--/col-xs-4-->
              
            </div><!--/row-->
             
            <div class="row">
            
              <div class="col-xs-4">    
                <div class="form-group">
                    <label for="">Teléfono Emergencia</label>
                    <input type="text" name="telefono_emergencia_usuario" class="form-control">
                </div>	
              </div><!--/col-xs-4-->
              
              <div class="col-xs-4">   
                <div class="form-group">
                    <label for="">Avisar a</label>
                    <input type="text" name="avisar_a_usuario" class="form-control">
                </div>	
              </div> <!--/col xs-4-->
              
             <div class="col-xs-4">   
                <div class="form-group">
                    <label for="">Email	</label>
                    <input type="text" name="" class="form-control">
                </div>
              </div> <!--/col xs-4-->  
              
            </div><!--row-->
            
        </div><!--/contacto-->    
           
    <div id="adicionales" class="tab-pane fade">
      <div class="row">
      
      	<div class="col-xs-4">
           <div class="form-group">
                 <label for="">Número Empleado</label>
                 <input type="text" name="numero_empleado" class="form-control" disabled value="<?php echo $numeroEmpleado ?>">
            </div>
         </div><!--/col-md-4-->
         
         <div class="col-xs-4">   
             <div class="form-group">
                    <label for="">Escolaridad</label>
                    <select class="form-control" name="escolaridad_usuario">
                        
                       <?php dropdownDinamico($escolaridad); ?>
                        
                    </select>
                </div>
           </div><!--/col-md-4-->
           
          <div class="col-xs-4">  
            <div class="form-group">
                <label for="">Puesto*</label>
                <select class="form-control" name="puesto">
                	<?php dropdownDinamico($puestos); ?>
                </select>
            </div>
          </div><!--/col-md-4-->
      </div><!--/row-->    
       
       <div class="row">  
       	<div class="col-xs-3">   
            <div class="form-group">
                <label for="">Departamento*</label>
                <select class="form-control" name="departamento_usuario" id="dropdownDepartamento">
                    <?php dropdownDinamico($departamentos); ?>
                </select>
            </div>
        </div><!--/col cs 3-->
         
        <div class="col-xs-3"> 
            <div class="form-group">
                <label for="">Area*</label>
                <select class="form-control" id="dropdownArea">
                
                </select>
            </div>
         </div><!--/col cs 3-->
         
         <div class="col-xs-3">   
            <div class="form-group">
                <label for="">DeHorario*</label>
                <input type="time" name="" class="form-control">
            </div>
         </div><!--/col cs 3-->  
            
         <div class="col-xs-3">     
            <div class="form-group">
                <label for="">AHorario*</label>
                <input  type="time" name="" class="form-control">
            </div>
         </div><!--/col cs 3--> 

       	</div><!--/row-->
    </div><!--/adicionales-->
</div><!--container-->


<input type="submit" class="btn btn-default" value="Agregar">

<?php echo form_close();?>

</div><!--/container-->

<script src="<?php echo base_url()?>assets/js/dropdownEstadosAjax.js">

</script>

<script>
$("#formUsuario").on("submit",function(){
	if($("#radioExtranjero").is(":checked")){
		$("#nacionalidad_usuario").val($("#nacionalidadText").val()) ;
	}
});
</script>


<?php $this->load->view("template/footer"); ?>