	<?php $this->load->view("template/header") ?>

<?php echo $this->session->flashdata("Error"); ?>

<div class="container login">
	<br>
    <img src="<?php echo base_url() ?>assets/img/sigma_logo.png" width="530" height="225" class="center-block spc-70px fondo-blanco">
    
    <div class="row">
      
      <div class="col-xs-4 col-md-push-4 spc-70px">
		<?php echo form_open("usuario/login", array("role" => "form")) ?>
        <div class="form-group">
           <?php echo form_input(array("class"=>"form-control","name"=>"nombre_usuario","placeholder" =>"Usuario..")); ?>
        </div>
        <div class="form-group">
        	<input type="password" class="form-control" name="clave_usuario" placeholder="Contraseña..">
        </div>
        <?php echo form_submit(array("class"=>"btn btn-default center-block","value"=>"Enviar")); ?>
        <?php echo form_close(); ?>
      </div>
      
    </div><!--row-->
    
    <div class="row img-logo spc-70px">
    
      <div class="col-xs-1">
    	<img src="<?php echo base_url() ?>assets/img/sigma-logo1.png" class="fondo-blanco">
      </div>
      
      <div class="col-xs-1 col-md-push-9">
    	<img src="<?php echo base_url() ?>assets/img/gloss-logo1.png" class="fondo-blanco">
      </div>
      
	</div><!--/row-->
    
</div><!--/container-->

<?php $this->load->view("template/footer"); ?>